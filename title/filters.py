from django_filters.rest_framework import FilterSet
from django_filters import filters

from .models import Title


class TitleFilter(FilterSet):
    vendor_name = filters.CharFilter(
        method='get_vendor_name',
        field_name='vendor_name'
    )
    studio_name = filters.CharFilter(
        method='get_studio_name',
        field_name='studio_name'
    )

    class Meta:
        model = Title
        fields = (
            'title_type',
            'studio_id',
            'studio_name',
            'vendor_id',
            'studio_name',
        )

    def get_vendor_name(self, queryset, name, value):
        return queryset.filter(vendor__name=value)

    def get_studio_name(self, queryset, name, value):
        return queryset.filter(studio__name=value)
