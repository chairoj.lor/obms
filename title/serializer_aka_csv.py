from rest_framework import serializers, exceptions

from title.models import Title


class AKACsvSerializer(serializers.Serializer):
    title_id = serializers.CharField(max_length=15)

    def validate_title_id(self, title_id):
        if Title.objects.filter(id=title_id).exists():
            return title_id
        raise exceptions.ValidationError('{} validate of title_id'.format(title_id))
