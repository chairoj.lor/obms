from rest_framework import mixins, status, viewsets
from rest_framework.response import Response

from .aka_title_export_script import AkaNameExport, AkaNameImport
from .models import Title
from .serializer import TitleListSerializer


class AkaExportView(viewsets.GenericViewSet, mixins.CreateModelMixin):
    queryset = Title.objects.none()
    serializer_class = TitleListSerializer
    pagination_class = None

    def create(self, request, *args, **kwargs):
        fields_list = (
            'ID',
            'AKA Title Name',
            'AKA Title Sysnopsis',
            'Title Name',
            'Episode ID',
            'Episode Name',
            'Sysnopsis',
            'AKA Episode Name',
            'AKA Episode Sysnopsis',
        )
        try:
            export_data = AkaNameExport(_title_id=request.title_id,_fields_list=fields_list)
            line_count = export_data.export_csv()
        except:
            line_count = 0
        return Response({"id": line_count}, status=status.HTTP_201_CREATED)


class AkaImportView(viewsets.GenericViewSet, mixins.CreateModelMixin):
    queryset = Title.objects.none()
    serializer_class = TitleListSerializer
    pagination_class = None

    def create(self, request, *args, **kwargs):
        fields_list = (
            'ID',
            'AKA Title Name',
            'AKA Title Sysnopsis',
            'Title Name',
            'Episode ID',
            'Episode Name',
            'Sysnopsis',
            'AKA Episode Name',
            'AKA Episode Sysnopsis',
        )
        try:
            import_aka = AkaNameImport(_title_id=request.title_id,_fields_list=fields_list)
            line_count = import_aka.read_csv()
        except:
            line_count = 0
        return Response({"id": line_count}, status=status.HTTP_201_CREATED)
