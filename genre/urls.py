from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import GenreView

router = DefaultRouter()

router.register(r'', GenreView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
