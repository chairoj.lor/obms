from django.urls import include, path
from rest_framework import routers

from .views import GenreView

router = routers.DefaultRouter()
router.register(r'', GenreView)

app_name = 'genre'
urlpatterns = [    
    path('', include(router.urls)),
]
