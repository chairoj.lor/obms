from rest_framework import viewsets

from .models import Role
from .serializers import RoleListSerializer


class RoleView(viewsets.ReadOnlyModelViewSet):
    queryset = Role.objects.all()
    serializer_class = RoleListSerializer
    pagination_class = None
