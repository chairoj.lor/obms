from .models import Role, AKA
from language.models import Language


# def init_role(id):
#     return Role.objects.get_or_create(id=id)


def get_role():
    return Role.objects.filter().first()


def init_role(id, name, language):
    Role.objects.get_or_create(id=id)
    language = Language.objects.get(code=language)    
    AKA.objects.get_or_create(role_id=id, name=name, language=language)
