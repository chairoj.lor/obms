from django.db import models


class Alert(models.Model):
    code_name = models.CharField(max_length=15)
    percent = models.IntegerField(default=0)
    response = models.TextField()
    backtrack = models.TextField()
    is_response = models.BooleanField(default=False)
    is_success = models.BooleanField(default=False)
    is_error = models.BooleanField(default=False)

    @property
    def results(self):
        try:
            from ast import literal_eval

            if self.is_error:
                return literal_eval(self.backtrack)
            if self.is_response:
                return literal_eval(self.response)
        except Exception as err:
            pass
        try:
            import json
            if self.is_error:
                return json.loads(self.backtrack)
            if self.is_response:
                return json.loads(self.response)
        except Exception as err:
            pass
        return None

    @property
    def value(self):
        item = self.results
        if not item:
            return {}
        if self.is_error:
            return {'error': self.backtrack}
        else:
            return item['results']

    @property
    def message(self):
        item = self.results
        if self.is_error:
            return self.backtrack
        if not item:
            return ""
        return item['message']