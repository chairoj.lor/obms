from rest_framework import serializers

from alert.models import Alert


class AlertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alert
        fields = (
            'id',
            'is_success',
            'is_response',
            'is_error',
            'percent',
            'value',
            'message'
        )
