from .models import System


def init_localization(type_of_localization):
    if System.objects.filter(value=type_of_localization, group_name="LOCALIZE_TYPE").exists():
        return System.objects.get(value=type_of_localization, group_name="LOCALIZE_TYPE")
    else:
        return System.objects.create(value=type_of_localization, group_name="LOCALIZE_TYPE")


def init_thumbnail(tag_of_thumbnail):
    return System.objects.get_or_create(value=tag_of_thumbnail, group_name="THUMNAIL_TAG")
