from rest_framework import exceptions, viewsets

from publish.models import Publish
from utils.rest_framework.permission import ViewPermission


class DetailGenericView(viewsets.GenericViewSet):
    app = 'publish'

    permission_classes_action = {
        'list': [ViewPermission],
        'retrieve': [ViewPermission],
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.publish = None

    def initial(self, request, *args, **kwargs):
        super().initial(request, *args, **kwargs)
        self.publish = Publish.pull(kwargs.get('publish_id'))
        if self.publish is None:
            raise exceptions.NotFound()

    def get_queryset(self):
        return super().get_queryset().filter(publish=self.publish)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]