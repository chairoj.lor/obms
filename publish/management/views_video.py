from rest_framework import mixins

from .serializers import VideoListSerializer
from .views_detail_generic import DetailGenericView
from ..models import Video


class VideoView(DetailGenericView, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    queryset = Video.objects.all().order_by('-resolution')
    serializer_class = VideoListSerializer
    pagination_class = None
    model = 'video'
