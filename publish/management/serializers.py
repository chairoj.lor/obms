from rest_framework import exceptions, serializers

from country.models import Country as Master
from language.models import Language
from publish.models import Audio, Country, Directory, Publish, SubTitle, Video
from utils.aes import AESEncrypt
from utils.rest_framework.serializer import ContentSerializer


class PublishSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)
    datetime_updated = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    update_by = serializers.CharField(source="account_updated.email", required=False, read_only=True)
    _is_merge = serializers.SerializerMethodField(method_name='convert_is_merge')

    class Meta:
        model = Publish
        fields = (
            'id',
            'desc',
            'is_merge',
            'datetime_created',
            'account_created',
            'create_by',
            'datetime_updated',
            'account_updated',
            'update_by',
            '_is_merge'
        )
        read_only_fields = ['id', ]

    def convert_is_merge(self, publish):
        if publish.is_merge == 'Y':
            return True
        else:
            return False


class AudioSerializer(serializers.Serializer):        
    language = serializers.CharField(max_length=3)    

    def validate_language(self, code):
        if Language.objects.filter(code=code).exists():
            return code
        raise exceptions.ValidationError()

class SubTitleSerializer(serializers.Serializer):        
    language = serializers.CharField(max_length=3)   

    def validate_language(self, code):
        if Language.objects.filter(code=code).exists():
            return code
        raise exceptions.ValidationError()


class VideoSerializer(serializers.Serializer):    
    codec = serializers.CharField(max_length=50)
    resolution = serializers.CharField(max_length=50)


class PublishCountrySerializer(serializers.Serializer):    
    country = serializers.CharField(max_length=3)

    def validate_country(self, code):
        if Master.objects.filter(code=code).exists():
            return code
        raise exceptions.ValidationError()    

class DirectorySerializer(serializers.Serializer):
    sequence_no = serializers.IntegerField(required=False)
    remote_ip = serializers.CharField(max_length=20)
    username = serializers.CharField(max_length=20)
    password = serializers.CharField(max_length=100)
    directory = serializers.CharField(max_length=20)

class PublishCreateSerializer(ContentSerializer, serializers.Serializer):
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)
    account_updated = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)
    desc = serializers.CharField(required=True, max_length=50)
    is_merge = serializers.CharField(required=False, allow_null=True, allow_blank=True, default="Y")
    audio_list = serializers.ListField(child=AudioSerializer())
    subtitle_list = serializers.ListField(child=SubTitleSerializer())
    video_list = serializers.ListField(child=VideoSerializer())
    country_list = serializers.ListField(child=PublishCountrySerializer())    
    directory_list = serializers.ListField(child=DirectorySerializer())    

    class Meta:
        model = Publish

    fields_map = {
        'desc': 'desc',
        'account_created': 'account_created',
        'account_updated': 'account_updated',
    }

    def validate_account_created(self, account):
        return self.context['request'].user

    def validate_account_updated(self, account):
        return self.context['request'].user

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_audio(validated_data, instance)
        self.create_subtitle(validated_data, instance)
        self.create_video(validated_data, instance)
        self.create_country(validated_data, instance)
        self.create_directory(validated_data, instance)

    def create_audio(self,  validated_data, publish):
        for row in validated_data.get('audio_list', []):
            if Audio.objects.filter(publish=publish, language_id=row['language']).exists():
                continue
            Audio.objects.create(
                publish=publish,
                language_id=row['language'],
                account_created=publish.account_created
            )
        language_id_list = map(lambda item: item['language'], validated_data.get('audio_list', []))
        language_id_list = list(language_id_list)
        Audio.objects.filter(publish=publish).exclude(language_id__in=language_id_list).delete()
        return True

    def create_subtitle(self,  validated_data, publish):
        SubTitle.objects.filter(publish=publish).delete()
        for row in validated_data.get('subtitle_list', []):
            if SubTitle.objects.filter(publish=publish, language_id=row['language'],).exists():
                continue
            SubTitle.objects.create(
                publish=publish,
                language_id=row['language'],
                account_created=publish.account_created
            )
        language_id_list = map(lambda item: item['language'], validated_data.get('subtitle_list', []))
        language_id_list = list(language_id_list)
        SubTitle.objects.filter(publish=publish).exclude(language_id__in=language_id_list).delete()
        return True

    def create_video(self,  validated_data, publish):
        Video.objects.filter(publish=publish).delete()
        for row in validated_data.get('video_list', []):             
            Video.objects.create(
                publish=publish,             
                codec=row['codec'],
                resolution=row['resolution'],
                account_created=publish.account_created
            )            
        return True

    def create_country(self,  validated_data, publish):
        for row in validated_data.get('country_list', []):
            if Country.objects.filter(publish=publish, country_id=row['country'],).exists():
                continue

            Country.objects.create(
                publish=publish,
                country_id=row['country'],
                account_created=publish.account_created
            )
        country_code_list = map(lambda item: item['country'], validated_data.get('country_list', []))
        country_code_list = list(country_code_list)
        Country.objects.filter(publish=publish).exclude(country_id__in=country_code_list).delete()
        return True

    def create_directory(self, validated_data, publish):
        sequence = 0        
        Directory.objects.filter(publish=publish).delete()
        for row in validated_data.get('directory_list', []):
            sequence += 1      
            Directory.objects.create(
                publish=publish,                    
                sequence_no=sequence,
                remote_ip=row['remote_ip'],
                username=row['username'],
                password=AESEncrypt(row['password'], row['remote_ip']).encrypt(),
                directory=row['directory'],
                account_created=publish.account_created
            )            
        return True

class AudioListSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Audio
        fields = (
            'language',
            'publish',
        )
        read_only_fields = ['id']

class SubTitleListSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubTitle
        fields = (
            'language',
            'publish',
            )

class VideoListSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)
    publish = serializers.CharField(source="publish.desc", required=False, read_only=True)
    class Meta:
        model = Video
        fields = (
            'codec', 
            'resolution',
            'datetime_created',
            'create_by',
            'publish'
            )

class PublishCountryListSerializer(serializers.ModelSerializer):   
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)
    publish = serializers.CharField(source="publish.desc", required=False, read_only=True)
    country_name = serializers.CharField(source="country.name", required=False, read_only=True)
    class Meta:
        model = Country
        fields = (
            'country', 
            'publish',
            'datetime_created',
            'create_by',
            'country_name'
            )

class PublishListSerializer(serializers.ModelSerializer):        
    _is_merge = serializers.SerializerMethodField(method_name='convert_is_merge')    

    class Meta:
        model = Publish
        fields = (
            'id',
            'desc',
            'is_merge',            
            '_is_merge',            
        )
        read_only_fields = ['id']

    def convert_is_merge(self, publish):
        if publish.is_merge == 'Y':
            return True
        else:
            return False

class DirectoryListSerializer(serializers.ModelSerializer):   
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)
    publish = serializers.CharField(source="publish.desc", required=False, read_only=True) 
    password = serializers.SerializerMethodField(method_name='convert_password')
    class Meta:
        model = Directory
        fields = (            
            'publish',
            'datetime_created',
            'create_by',
            'sequence_no',
            'remote_ip',
            'username',
            'password',
            'directory'
            )

    def convert_password(self, directory):
        if directory.password != '':
            return AESEncrypt(directory.password,directory.remote_ip).decrypt()
        return ''



