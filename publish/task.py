import json
from datetime import timedelta

from celery import shared_task
from django.db.models import F, Q

from account.models import Account
from alert.models import Alert
from episode.models import Episode, PublishEpisode
from publish.serializers_schedule import ScheduleSerializer
from publish.serializers_tools import ToolsSerializer


def generate_datetime_slot(datetime_start, title_id, start_pac_no, end_pac_no,  shift_days, select_of_weekdays, **kwargs):
    if select_of_weekdays is None:
        select_of_weekdays = [0, 1, 2, 3, 4, 5, 6]
    datetime_slot_list = []
    count_slot = Episode.objects.filter(
        title_id=title_id,
        pac_no__range=(start_pac_no, end_pac_no)
    ).count()
    last_index_select_week = len(select_of_weekdays) - 1
    week_current_index = 0
    count_day = 0
    for slot in range(count_slot):
        if week_current_index > last_index_select_week:
            week_current_index = 0
            count_day =  shift_days  + count_day
        while True:
            datetime_publish = datetime_start + timedelta(count_day)
            if datetime_publish.weekday() == select_of_weekdays[week_current_index]:
                datetime_slot_list.append(datetime_publish)
                week_current_index += 1
                break
            count_day = count_day + 1
    return datetime_slot_list


def set_episode_datetime_slot(
        episode_list,
        datetime_slot_list,
        time_start,
        days_expire,
        time_expire,
        publish_zone_ids,
        datetime_expire,
        datetime_start=None,
):
    item_time_slot_list = []
    episode_datetime_conflict_of_slot = {}
    is_schedule = len(datetime_slot_list) > 0
    if not is_schedule and datetime_start is None:
        raise Exception('Publish tools required datetime_start')
    for index_of_episode, episode in enumerate(episode_list):
        if is_schedule:
            datetime_start = datetime_slot_list[index_of_episode]
            datetime_start = datetime_start + timedelta(
                hours=time_start.hour,
                minutes=time_start.minute,
                seconds=time_start.second,
                microseconds=time_start.microsecond,
            )
        if is_schedule:
            datetime_expire = datetime_expire if datetime_expire else datetime_start + timedelta(days=days_expire)
            datetime_expire = datetime_expire + timedelta(
                hours=time_expire.hour,
                minutes=time_expire.minute,
                seconds=time_expire.second,
                microseconds=time_expire.microsecond,
            )
        for publish_zone_id in publish_zone_ids: # validate zone publish
            episode_datetime_period_list = episode.publishepisode_set.filter(  # filter by period
                Q(datetime_start__range=(datetime_start, datetime_expire)) |
                Q(datetime_end__range=(datetime_start, datetime_expire))
            )
            episode_id = getattr(episode, 'id')
            is_publish_zone = not episode_datetime_period_list.filter(publish_id=publish_zone_id).exists()
            assert is_publish_zone, 'This time slot episode %s already exists.' % episode_id
            period_episode_publish_key = "%s_%s" % (episode_id, publish_zone_id)
            if period_episode_publish_key in episode_datetime_conflict_of_slot: # validate episode
                episode_datetime_list = episode_datetime_conflict_of_slot.get(period_episode_publish_key, [])
                if len(episode_datetime_list) > 1:
                    index_episode_datetime_last = len(episode_datetime_conflict_of_slot) - 1
                    for index_episode_datetime, datetime_period in enumerate(episode_datetime_list):
                        if index_episode_datetime < index_episode_datetime_last:
                            datetime_period_next = episode_datetime_list[index_episode_datetime + 1]
                            assert datetime_period_next['datetime_start'] > datetime_period['datetime_end'], 'This time slot episode %s already exists.' % episode_id

                datetime_period = {
                    'datetime_start': datetime_start,
                    'datetime_expire': datetime_expire
                }
                episode_datetime_list.append(datetime_period)
                episode_datetime_conflict_of_slot.update({period_episode_publish_key: episode_datetime_list})
            else:
                episode_datetime_conflict_of_slot.update(
                    {
                        period_episode_publish_key: [
                            {
                                'datetime_start': datetime_start,
                                'datetime_expire': datetime_expire
                            }
                        ]
                    }
                )
            item_time_slot_list.append(
                {
                    'episode_id': episode.id,
                    'publish_zone_id': publish_zone_id,
                    'publish_datetime_start': datetime_start,
                    'publish_datetime_expire': datetime_expire,
                }
            )
    return item_time_slot_list


def get_and_set_episode_publish_list(title_id,
                                     days_expire,
                                     datetime_expire,
                                     datetime_start,
                                     time_expire,
                                     publish_zone_ids,
                                     start_pac_no,
                                     end_pac_no,
                                     datetime_slot_list, **kwargs):
    episode_list = Episode.objects \
        .filter(title_id=title_id, pac_no__range=(start_pac_no, end_pac_no)) \
        .order_by('pac_no')
    assert episode_list.count() <= len(datetime_slot_list), 'Time slots are not just for episode.'
    try:
        item_time_slot_list = set_episode_datetime_slot(
            episode_list,
            datetime_slot_list,
            datetime_start.time(),
            days_expire,
            time_expire,
            publish_zone_ids,
            datetime_expire,
        )
        return item_time_slot_list
    except Exception as err:
        raise err


def get_publish_sequent_number(episode_id, publish_id, datetime_start):
    sequent_number = 1
    publish_list = PublishEpisode.objects.filter(
        episode_id=episode_id,
        publish_id=publish_id
    ).order_by('datetime_start')
    if publish_list.exists():
        publish_periods_list = publish_list.filter(datetime_start__gt=datetime_start)
        if publish_periods_list.exists():
            publish = publish_periods_list.first()
            return publish.sequence_no, 'update'
        else:
            publish = publish_list.last()
            return publish.sequence_no + 1, 'append'

    return sequent_number, 'insert'


def create_episode(item_time_slot_list, account, response, alert_id):
    item_time_slot_size = len(item_time_slot_list)
    for index_of_item, item_time in enumerate(item_time_slot_list):
        sequence_no, action = get_publish_sequent_number(
            item_time['episode_id'],
            item_time['publish_zone_id'],
            item_time['publish_datetime_start']
        )
        if action == 'update':
            PublishEpisode.objects.filter(
                episode_id=item_time['episode_id'],
                publish_id=item_time['publish_zone_id'],
                sequence_no__gte=sequence_no,
            ).update(
                sequence_no=F('sequence_no') + 1
            )
        publish = PublishEpisode.objects.create(
            episode_id=item_time['episode_id'],
            publish_id=item_time['publish_zone_id'],
            datetime_start=item_time['publish_datetime_start'],
            datetime_end=item_time['publish_datetime_expire'],
            sequence_no=sequence_no,
            account_created=account,
        )
        percent_of_item = ((index_of_item + 1) // item_time_slot_size) // 100
        percent = (98 // 100) * percent_of_item
        response.update(
            {
                'message': 'Episode `{}` map publish `{}` to `{}` '.format(
                    publish.episode_id,
                    publish.datetime_start.strftime('%c'),
                    publish.datetime_end.strftime('%c')
                )
            }
        )
        Alert.objects.filter(id=alert_id).update(percent=percent, response=json.dumps(response))
    response.update({'message': 'Generate publish successfully.'})
    Alert.objects.filter(id=alert_id).update(percent=100, is_success=True, response=json.dumps(response))


@shared_task
def generate_episode_schedule(data, alert_id, account_id):
    try:
        account = Account.objects.get(id=account_id)
        response = {'message': "validate data ....", 'results': [], 'error': ""}
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=10)
        serializer = ScheduleSerializer(data=data)
        serializer.is_valid(raise_exception=True)
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err

    try:
        response.update({'message': 'Validate and generate slot of datetime'})
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=20)
        data = serializer.validated_data
        datetime_slot_list = generate_datetime_slot(**data)
        data.update({'datetime_slot_list': datetime_slot_list})
        assert len(datetime_slot_list) > 0, 'Datetime slot publish is empty.'
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err

    try:
        response.update({'message': 'Validate and map datetime periods of episode.'})
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=50)

        item_time_slot_list = get_and_set_episode_publish_list(**data)
        assert len(item_time_slot_list) > 0, 'Episode map datetime publish is empty'
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err
    try:
        create_episode(item_time_slot_list, account, response, alert_id)
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err


@shared_task
def generate_episode_tools(data, alert_id, account_id):
    try:
        account = Account.objects.get(id=account_id)
        response = {'message': "validate data ....", 'results': [], 'error': ""}

        serializer = ToolsSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        data = serializer.validated_data
        data.update({'account': account, 'alert_id': alert_id})
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=10)
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err

    try:

        episode_list = Episode.objects.filter(id__in=data['episode_id_list'])
        response = {'message': "generate slot time  ....", 'results': [], 'error': ""}
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=20)

        data.update({'datetime_slot_list': [], 'episode_list': episode_list})
        response = {'message': "Episode mapping ....", 'results': [], 'error': ""}
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=40)
        item_time_slot_list = set_episode_datetime_slot(
            episode_list,
            [],
            None,
            None,
            None,
            data['publish_zone_ids'],
            data['datetime_end'],
            data['datetime_start'],
        )
        response = {'message': "Episode map slot time success ....", 'results': [], 'error': ""}
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=45)
        assert len(item_time_slot_list) > 0, 'Episode map datetime publish is empty'
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err

    try:
        response = {'message': "Episode mapping publish zone ....", 'results': [], 'error': ""}
        Alert.objects.filter(id=alert_id).update(is_response=True, response=json.dumps(response), percent=50)
        create_episode(item_time_slot_list, account, response, alert_id)
    except Exception as err:
        Alert.objects.filter(id=alert_id).update(is_error=True, backtrack=err)
        raise err
