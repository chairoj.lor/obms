from rest_framework import exceptions, serializers

from deal.models import Territory, Video
from episode.models import Episode
from publish.models import Country, Publish


class ToolsSerializer(serializers.Serializer):
    episode_id_list = serializers.ListField(
        child=serializers.CharField(
            max_length=15
        )
    )
    publish_zone_ids = serializers.ListField(
        child=serializers.CharField(max_length=15)
    )
    datetime_start = serializers.DateTimeField()
    datetime_end = serializers.DateTimeField()

    def validate(self, attrs):

        publish_zone_ids = attrs['publish_zone_ids']
        episode_id_list = attrs['episode_id_list']
        title_id_list = Episode.objects.filter(
            id__in=episode_id_list,
        ).values_list('title_id', flat=True)
        datetime_start = attrs['datetime_start']
        datetime_end = attrs['datetime_end']

        video_title_list = Video.objects.filter(
            title_id__in=title_id_list,
            datetime_right_start__lte=datetime_start,
            datetime_right_end__gte=datetime_end
        )
        if not video_title_list.exists():
            raise exceptions.ValidationError(
                detail={
                    'episode_id_list': [
                        'Title of episode license from {} to {} not found.'.format(datetime_start.strftime("%c"),
                                                                                   datetime_end.strftime("%c")),
                    ]
                }
            )
        country_publish_list = Country.objects.filter(publish_id__in=publish_zone_ids)
        if not country_publish_list.exists():
            raise exceptions.ValidationError(
                detail={'publish_zone_ids': [
                    'Publish zone of country is not found.']},
            )

        title_territories = Territory.objects.filter(deal__video__title_id__in=title_id_list)
        for country_publish in country_publish_list:
            if title_territories.filter(country=country_publish.country).exists():
                continue
            raise exceptions.ValidationError(
                detail={
                    'publish_zone_ids': [
                        'Publish of territory of country {} is not exists'.format(country_publish.country_id)],
                }
            )
        if datetime_end:
            if not video_title_list.filter(
                    datetime_right_start__lte=datetime_end,
                    datetime_right_end__gte=datetime_end
            ).exists():
                raise exceptions.ValidationError(
                    {'datetime_expired': ['Expired datetime {} is not periods'.format(datetime_end.strftime('%c'))]})
        return attrs

    def validate_episode_id_list(self, episode_id_list):
        for episode_id in episode_id_list:
            if Episode.objects.filter(id=episode_id).exists():
                continue
            raise exceptions.ValidationError('{} episode is not found.'.format(episode_id))
        return episode_id_list

    def validate_publish_zone_ids(self, publish_zone_ids):
        for publish_zone_id in publish_zone_ids:
            if Publish.objects.filter(id=publish_zone_id).exists():
                continue
            raise exceptions.ValidationError('{} publish_zone_id is not found.'.format(publish_zone_id))
        return publish_zone_ids
