from rest_framework import exceptions, serializers

from sub_genre.models import SubGenre

class SubGenreListSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)

    class Meta:
        model = SubGenre
        fields = (
            'id',
            'name',
            'datetime_created',
            'account_created',
            'create_by',            
        )


class SubGenreSerializer(serializers.ModelSerializer):
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)

    class Meta:
        model = SubGenre
        fields = (
            'id',
            'name',
            'account_created'
        )
        read_only_fields = ['id']

    def validate_account_created(self, account):
        return self.context['request'].user

    def validate_name(self, name):
        if SubGenre.objects.filter(name=name).exists():
            raise  exceptions.ValidationError('Duplicate %s ' % name)
        return name