import binascii
import os

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import Group, Permission, PermissionsMixin
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models.py here.


class AccountManager(BaseUserManager):
    def create_user(self, email, password, employee_id):
        if email is None:
            raise ValueError('The given username must be set')

        user = self.model(
            email=email, 
            employee_id=employee_id,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, employee_id):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """

        user = self.create_user(email, password, employee_id)
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)
        return user

    def get_by_natural_key(self, username):
        return self.get(email=username)


class Account(AbstractBaseUser, PermissionsMixin):
    employee_id = models.CharField(max_length=120, db_index=True, unique=True)
    email = models.EmailField(
        verbose_name='Email address',
        max_length=255,
        db_index=True,
        unique=True,
    )

    first_name = models.CharField(max_length=120, db_index=True, blank=True)
    last_name = models.CharField(max_length=120, db_index=True, blank=True)
    group_active = models.ForeignKey(Group, null=True, blank=True, default=None, on_delete=models.SET_NULL)
    last_login = models.DateTimeField(_('last login'), blank=True, null=True)
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )

    # EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['employee_id']
    objects = AccountManager()

    class Meta:
        ordering = ['-id']
        default_permissions = (
            'view',
            'add',
            'change',
            'delete',
        )

    @staticmethod
    def pull(pk):
        from .cacheds import get_account
        account = get_account(pk)
        if account:
            return account
        else:
            return Account.objects.filter(id=pk).first()

    def grant_permission_publish(self):
        self.grant_permission(settings.PUBLISH_APPS)

    def grate_permission_private(self):
        self.grant_permission(settings.PRIVATE_APPS)

    def grant_permission_business(self):
        self.grant_permission(settings.BUSINESS_APPS)

    def grant_permission_master(self):
        self.grant_permission(settings.MASTER_APPS)

    def grant_permission(self, app_list):
        for content_type in ContentType.objects.filter(app_label__in=app_list):
            for permission in Permission.objects.filter(content_type=content_type):
                if self.user_permissions.filter(codename=permission.codename, content_type=content_type).exists():
                    continue
                self.user_permissions.add(permission)

    def grant_management_permission(self):
        app_setting = settings.TEMP_APPS[1]
        for custom_permission in app_setting['permission_list']:
            content_type = settings.CONTENT_TYPE(custom_permission['content_type_code'])
            if content_type:
                for permission in Permission.objects.filter(content_type=content_type):
                    if self.user_permissions.filter(codename=permission.codename, content_type=content_type).exists():
                        continue
                    else:
                        self.user_permissions.add(permission)

    def grant_aka_permission(self):
        app_setting = settings.TEMP_APPS[0]
        for custom_permission in app_setting['permission_list']:
            codename_list = custom_permission.get('codename', None)
            if isinstance(codename_list, list):
                for codename_temp in codename_list:
                    content_type = settings.CONTENT_TYPE(custom_permission.get('content_type_code', ''))
                    codename = codename_temp.split('.').pop()
                    if self.user_permissions.filter(codename=codename).exists():
                        continue
                    else:
                        permission = Permission.objects.get(codename=codename, content_type=content_type)
                        self.user_permissions.add(permission)

    def drop_permission_private(self):
        self.drop_permission(settings.PRIVATE_APPS)

    def drop_permission_publish(self):
        self.drop_permission(settings.PUBLISH_APPS)

    def drop_permission_master(self):
        self.drop_permission(settings.MASTER_APPS)

    def drop_permission_business(self):
        self.drop_permission(settings.BUSINESS_APPS)

    def drop_permission(self, app_list):
        for permission in self.user_permissions.filter(content_type__app_label__in=app_list):
            self.user_permissions.remove(permission)

    def drop_permission_all(self, ):
        for permission in self.user_permissions.all():
            self.user_permissions.remove(permission)

    def hash_prem_group(self, content_type, codename):
        return self.groups.filter(permissions__codename=codename, permissions__content_type=content_type).exists()

    def get_permission_display(self):
        permissions_list = {}
        permissions_app = []
        for permissions in list(self.get_all_permissions()):
            app = permissions.split('.')[0]
            action = permissions.split('.')[1].split('_')[0]
            model = permissions.split('.')[1].split('_')[1]
            is_main = action == 'view'
            if app in permissions_app:
                permissions_item = permissions_list.get(app)
                if not permissions_item.get('is_allow', False) and is_main:
                    permissions_item.update({'is_allow': is_main})
                module = permissions_item.get('module', [])
                is_allow = self.has_perm(permissions)
                code = '{}.{}'.format(app, model)
                module_item = {
                    'codename': permissions,
                    'code': code,
                    'is_allow': is_allow,
                    'key': action,
                }
                module.append(module_item)
            else:
                is_allow = self.has_perm(permissions)
                code = '{}.{}'.format(app, model)
                permissions_item = {
                    'is_allow': is_main,
                    'module': [
                        {
                            'codename': permissions,
                            'code': code,
                            'is_allow': is_allow,
                        }
                    ]
                }
                permissions_list.update({str(app): permissions_item})
                permissions_app.append(app)
        permissions = []
        for app in settings.PUBLISH_APPS:
            item = permissions_list.get(app, {})
            temp = {
                'codename': '{}.all_{}'.format(app, app),
                'code': '{}.{}'.format(app, app),
                'is_allow': False,
                'module': []
            }
            temp.update(item)
            permissions.append(temp)
        for custom_permission in settings.TEMP_APPS:
            is_allow_permission = True
            for code_info in custom_permission['permission_list']:
                codename = code_info.get('codename', None)
                if codename:
                    for code in code_info['codename']:
                        if not self.has_perm(code):
                            is_allow_permission = False
                            break
                else:
                    is_allow_permission = False
                    content_type_code = code_info.get('content_type_code', '')
                    for permission in Permission.objects.filter(content_type=settings.CONTENT_TYPE(content_type_code)):
                        if self.has_perm(permission):
                            is_allow_permission = True
                            break
            temp = {
                'app': custom_permission['app'],
                'code': '{}.{}'.format(custom_permission['name'], custom_permission['app']),
                'is_allow': is_allow_permission,
                'module': []
            }
            permissions.append(temp)
        return permissions


class Token(models.Model):
    TYPE_CHOICES = (
        (1, 'web'),
        (2, 'application'),
        (3, 'remote'),
    )

    key = models.CharField(_("Key"), max_length=255, primary_key=True)
    account = models.OneToOneField(
        Account, related_name='auth_token',
        on_delete=models.CASCADE, verbose_name=_("Account")
    )
    type = models.PositiveIntegerField(default=1, choices=TYPE_CHOICES)
    is_expired = models.BooleanField(default=False)
    datetime_created = models.DateTimeField(_("Created"), auto_now_add=True)

    class Meta:
        default_permissions = (
            'view',
            'view_detail',
            'add',
            'change',
            'delete'
        )
        verbose_name = _("Token")
        verbose_name_plural = _("Tokens")

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    def generate_key(self):
        return binascii.hexlify(os.urandom(32)).decode()

    def __str__(self):
        return self.key
