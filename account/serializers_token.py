# from google.auth.transport import requests

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from rest_framework import exceptions
from rest_framework import serializers

from utils.google import GoogleAuthenticate
from .models import Account, Token
from .serializers_account import AccountSerializer


class GoogleTokenSerializer(serializers.Serializer):
    token = serializers.CharField(required=True, max_length=255, min_length=15)
    email = serializers.EmailField(required=False, allow_blank=True, allow_null=True)

    def validate(self, attrs):
        token = attrs['token']
        google = GoogleAuthenticate()
        if not google.is_valid_token(token):
            raise exceptions.AuthenticationFailed("Validate key")
        user = Account.objects.filter(email=google.data['email']).first()
        if user is None:
            raise exceptions.AuthenticationFailed()
        attrs.update({'email': user.email})
        return attrs


class TokenAccountSerializer(serializers.ModelSerializer):
    account = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()

    class Meta:
        model = Token
        fields = (
            "token",
            "account"
        )

    def get_token(self, instance):
        return instance.key

    def get_account(self, instance):
        account_serializer = AccountSerializer(instance=instance.account)
        return account_serializer.data
