from django.contrib.auth.models import Group
from rest_framework import serializers


class GroupPermissionSerializer(serializers.ModelSerializer):
    is_allow = serializers.SerializerMethodField()
    permission_code = serializers.SerializerMethodField()
    app_code = serializers.SerializerMethodField()
    app_allow = serializers.SerializerMethodField()

    class Meta:
        model = Group
        fields = (
            'id',
            'name',
            'is_allow',
            'permission_code',
            'app_code',
            'app_allow',
        )

    def get_is_allow(self, group):
        _self_view = self.context['view']
        return _self_view.request.user.groups.filter(id=group.id).exists()

    def get_permission_code(self, group):
        _self_view = self.context['view']
        _permission_all = []
        for _permission in group.permissions.all():
            code = '%s_%s_%s' % (_permission.content_type.app_label, _permission.content_type.model, _permission.codename)
            _permission_all.append(code)
        return _permission_all

    def get_app_code(self, group):
        _permission_all = []
        for _permission in group.permissions.all():
            app_code = 'app.%s.%s' % (_permission.content_type.app_label, _permission.content_type.model)
            _permission_all.append(app_code)
        return _permission_all

    def get_app_allow(self, group):
        _permission_all = []
        account = self.context['request'].user
        for _permission in group.permissions.all():
            app_code = 'app.%s.%s' % (_permission.content_type.app_label, _permission.content_type.model)
            user_permission = {app_code: account.has_perm('%s.%s' % (_permission.content_type.app_label, _permission.codename))}
            _permission_all.append(user_permission)
        return _permission_all

