from uuid import uuid4

from django.core.management import call_command
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from utils.google import GoogleAuthenticate
from .fixture import create_user
from .models import Token


class AuthenticateTestCase(APITestCase):
    def setUp(self) -> None:

        call_command('loaddata', 'account/fixtures/initial_data.json', verbosity=0)
        self.google = GoogleAuthenticate()
        self.path = '/api/account/authenticate'
        self.client = APIClient()

    def test_get_uri_google(self):
        response = self.client.get(self.path + '/google/')
        self.assertEqual(response.data['redirect_uri'], self.google.get_url_authenticate())
        self.assertEqual(response.data['response_type'], self.google.get_response_type())
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_token_login(self):
        token = uuid4().hex
        response = self.client.post(self.path + '/google/', data={"token": token}, json=True)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIsNotNone(Token.objects.filter(key=response.data['token']).first())


class ProfileTestCase(APITestCase):
    def setUp(self) -> None:
        call_command('loaddata', 'account/fixtures/initial_data.json', verbosity=0)
        self.account = create_user()
        self.google = GoogleAuthenticate()
        self.path = '/api/account/profile/'
        self.client = APIClient()
        self.token, is_token = Token.objects.get_or_create(account=self.account)
        self.client.force_authenticate(user=self.account, token=self.token)

    def test_get_profile(self):
        response = self.client.get(self.path)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['email'], self.account.email)
