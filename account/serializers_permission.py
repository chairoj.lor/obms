from django.contrib.auth.models import Permission
from rest_framework.serializers import ModelSerializer, SerializerMethodField


class PermissionSerializer(ModelSerializer):
    app_code = SerializerMethodField()

    class Meta:
        model = Permission
        fields = (
            'codename',
            'app_code',
        )

    def get_app_code(self, permission):
        return 'app.%s.%s' % (permission.content_type.app_label, permission.content_type.model)
