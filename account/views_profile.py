from rest_framework import mixins, permissions, viewsets
from rest_framework.response import Response

from .models import Account
from .serializers_account import AccountSerializer


class ProfileView(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    pagination_class = None
    queryset = Account.objects.none()
    serializer_class = AccountSerializer
    permission_classes = [
        permissions.IsAuthenticated
    ]

    def get_queryset(self):
        return Account.objects.filter(id=self.request.user.id)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset.first())
        return Response(serializer.data)






