from django.contrib.auth import logout
from rest_framework import status, views
from rest_framework.response import Response

from .models import Token


class LogoutView(views.APIView):

    def post(self, request, format=None):
        Token.objects.filter(account=request.user, type=1).delete()
        logout(request)
        return Response({"message": "logout success"}, status=status.HTTP_200_OK)