from django.contrib import admin

from .models import Asset


@admin.register(Asset)
class AssetModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'file_name', 'datetime_created')
    search_fields = ('id', 'file_name',)
