from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import CostView

router = DefaultRouter()
router.register(r'', CostView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
