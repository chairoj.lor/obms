from django.conf import settings
from django.db import models

from utils.fields import SaveMixin


class Cost(SaveMixin, models.Model):
    id = models.CharField(max_length=15, primary_key=True, db_column="COST_ID")
    name = models.CharField(max_length=50, db_column="COST_NAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    prefix = "SC"
    primary_key = "id"

    class Meta:
        ordering = ('name', )
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_COST')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.id, self.name)
