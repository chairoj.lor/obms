from rest_framework import exceptions, mixins, status, viewsets
from rest_framework.response import Response

from ..models import Structure
from .serializers import StructureListSerializer, StructureSerializer


class StructureView(
    viewsets.GenericViewSet,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin
):
    queryset = Structure.objects.none()
    serializer_class = StructureListSerializer
    pagination_class = None
    action_serializers = {
        'list': StructureListSerializer,
        'retrieve': StructureListSerializer,
        'destroy': StructureSerializer,
        'create': StructureSerializer,
        'update': StructureSerializer,
        'partial_update': StructureSerializer,
    }

    def get_queryset(self):
        if self.action == 'list':
            return Structure.objects.filter(parent__isnull=True)
        else:
            return Structure.objects.all()

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance.get_child(), many=True)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        structure = self.get_object()
        if structure.get_child().exists():
            raise exceptions.ValidationError('{} is use structure'.format(structure.id))
        structure.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
