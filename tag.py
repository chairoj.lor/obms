import os
from pygit2 import Repository



def get_tag():
    repo = Repository(os.getcwd())
    head = repo.head.name
    branch = head.replace("refs/heads/", "")
    return branch.replace("/", "-")




if __name__ == '__main__':
    tag = get_tag()
    print("subaruqui/bms:{}".format(tag))

