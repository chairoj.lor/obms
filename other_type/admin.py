from django.contrib import admin
from .models import Type

@admin.register(Type)
class TypeModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    search_fields = ['id', 'name']
