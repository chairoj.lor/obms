from django.apps import AppConfig


class OtherTypeConfig(AppConfig):
    name = 'other_type'
