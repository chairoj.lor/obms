from rest_framework import serializers

from other_type.models import Type


class OtherTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = (
            'id',
            'name'
        )