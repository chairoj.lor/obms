from rest_framework import serializers

from ..models import Language


class LanguageListSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)
    datetime_updated = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    update_by = serializers.CharField(source="account_updated.email", required=False, read_only=True)
    _is_localize = serializers.SerializerMethodField(method_name='convert_localize')
    _is_aka = serializers.SerializerMethodField(method_name='convert_aka')


    class Meta:
        model = Language
        fields = (
            'code',
            'name',
            'alpha2',
            'is_localize',
            'is_aka',
            '_is_localize',
            '_is_aka',
            'datetime_created',
            'account_created',
            'create_by',
            'datetime_updated',
            'account_updated',
            'update_by'
        )

    def convert_localize(self, language):
        if language.is_localize == 'Y':
            return True
        else:
            return False
    
    def convert_aka(self, language):
        if language.is_aka == 'Y':
            return True
        else:
            return False

class LanguageSerializer(serializers.ModelSerializer):
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)
    account_updated = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)

    class Meta:
        model = Language
        fields = (
            'code',
            'name',
            'alpha2',
            'account_created',
            'account_updated'

        )

    def validate_account_created(self, account):
        return self.context['request'].user

    def validate_account_updated(self, account):
        return self.context['request'].user


     