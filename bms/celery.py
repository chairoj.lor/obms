import os

from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'bms.settings')
CONFIG_ENV_MQ = {
    'rabbit_user': os.getenv('RABBIT_USER', 'guest'),
    'rabbit_pass': os.getenv('RABBIT_PASS', 'guest'),
    'rabbit_host': os.getenv('RABBIT_HOST', '127.0.0.1'),
    'rabbit_port': os.getenv('RABBIT_PORT', '5672')
}
mq_uri = 'amqp://{rabbit_user}:{rabbit_pass}@{rabbit_host}:{rabbit_port}'.format(**CONFIG_ENV_MQ)

app = Celery('bms', broker=mq_uri)
app.config_from_object('django.conf:settings', namespace=os.getenv('ENV'))
app.autodiscover_tasks()
