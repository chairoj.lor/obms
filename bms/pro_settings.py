import os
import sys

ENV_NAME_LIST = [
    'ORA_HOST',
    'ORA_PORT',
    'ORA_SID',
    'ORA_USER',
    'ORA_PASS',
    'RABBIT_HOST',
    'RABBIT_PORT',
    'RABBIT_USER',
    'RABBIT_PASS',
    'CACHED_SERVICE',
]

for key in ENV_NAME_LIST:
    val = os.getenv(key)
    if val:
        continue
    print(key, ' required env name and value')
    sys.exit(1)

ORA_INFO = {
    'host': os.getenv("ORA_HOST"),
    'port': os.getenv("ORA_PORT"),
    'sid': os.getenv("ORA_SID")
}

ORA_NAME = "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = {host})(PORT = {port})) (CONNECT_DATA = (SID = {sid}) ) )".format(
    **ORA_INFO
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': ORA_NAME,
        'USER': os.getenv("ORA_USER"),
        'PASSWORD': os.getenv("ORA_PASS")
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.PyLibMCCache',
        'LOCATION': os.getenv("CACHED_SERVICE", None),
    },

}