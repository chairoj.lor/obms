from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, viewsets

from .models import Country
from .serializers import CountrySerializer


class CountryView(viewsets.ReadOnlyModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    pagination_class = None
    filter_backend = [DjangoFilterBackend, filters.BaseFilterBackend]

