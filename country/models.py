from django.db import models

from bms import settings
from utils.fields import ObmModel

IS_ACTIVE = 'Y'
IN_ACTIVE = 'N'
ACTIVE_CHOICES = (
    (IS_ACTIVE, True),
    (IN_ACTIVE, False),
)


class Country(ObmModel):
    code = models.CharField(max_length=15, primary_key=True, unique=True, db_column='COUNTRY_CODE')
    name = models.CharField(max_length=50, db_column="COUNTRY_NAME")

    class Meta:
        ordering = ('name', )
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_COUNTRY')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.code, self.name)
