from datetime import timedelta

from django_filters import filters
from django_filters.rest_framework import FilterSet

from .models import Schedule


class EpgFilter(FilterSet):
    datetime_start = filters.DateTimeFromToRangeFilter(method="get_datetime_start")

    class Meta:
        model = Schedule
        fields = ['datetime_start', 'bms_code']

    def get_datetime_start(self, queryset, field_name, value):
        if value.start and value.stop:
            datetime_start = value.start + timedelta(hours=7)
            datetime_end = value.stop + timedelta(hours=7)
            return queryset.filter(datetime_start__range=(datetime_start, datetime_end))
        else:
            return queryset
