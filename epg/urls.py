from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import EpgScheduleView

router = DefaultRouter()

router.register(r'', EpgScheduleView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
