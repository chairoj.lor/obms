from datetime import timedelta

from rest_framework import serializers
from channel.models import Channel
from .models import Schedule


class EpgSerializer(serializers.ModelSerializer):
    duration = serializers.SerializerMethodField()
    channel_name = serializers.SerializerMethodField()
    datetime_start = serializers.SerializerMethodField()
    datetime_end = serializers.SerializerMethodField()

    class Meta:
        model = Schedule
        fields = (
            'datetime_start',
            'datetime_end',
            'episode_id',
            'episode_name',
            'title_id',
            'title_eng',
            'title_tha',
            'channel_name',
            'bms_code',
            'duration',
        )

    def get_duration(self, schedule):
        duration = schedule.datetime_end - schedule.datetime_start
        hours = int(duration.total_seconds() // 3600)
        minutes = int((duration.total_seconds() % 3600) // 60)
        seconds = int((duration.total_seconds() % 3600) % 60)
        hours = hours if hours > 9 else '0%d' % hours
        minutes = minutes if minutes > 9 else '0%d' % minutes
        seconds = seconds if seconds > 9 else '0%d' % seconds
        return '%s:%s:%s' % (hours, minutes, seconds)

    def get_channel_name(self, schedule):
        channels = Channel.pull_bms(schedule.bms_code)
        if channels.exists():
            name_list = channels.values_list('name', flat=True)
            name_list = list(name_list)
            return ','.join(name_list)
        return ''

    def get_datetime_start(self, schedule):
        return schedule.datetime_start

    def get_datetime_end(self, schedule):
        return schedule.datetime_end

