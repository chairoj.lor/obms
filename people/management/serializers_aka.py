from rest_framework import serializers

from people.models import AKA


class ListAKASerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)    
    create_by = serializers.CharField(source='account_created.email', required=False, read_only=True)
    language_name = serializers.CharField(source='language.name', required=False, read_only=True)    

    class Meta:
        model = AKA
        fields = (            
            'name',
            'language',
            'account_created',
            'datetime_created',
            'create_by',
            'language_name',            
        )
