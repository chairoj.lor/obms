from rest_framework import exceptions, serializers

from language.models import Language
from people.models import AKA, People
from utils.rest_framework.serializer import ContentSerializer


class PeopleSerializer(serializers.ModelSerializer):
    datetime_created = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S", required=False, read_only=True)
    create_by = serializers.CharField(source="account_created.email", required=False, read_only=True)

    class Meta:
        model = People
        fields = (
            'id',
            'name',
            'datetime_created',
            'account_created',
            'create_by'
        )
        read_only_fields = ['id', ]


class PeopleAKASerializer(serializers.Serializer):    
    name = serializers.CharField(max_length=50, allow_null=True, allow_blank=True, default=None)
    language = serializers.CharField(max_length=3)    

    def validate_language(self, code):
        if Language.objects.filter(code=code, is_aka='Y').exists():
            return code
        raise exceptions.ValidationError()

class PeopleCreateSerializer(ContentSerializer, serializers.Serializer):
    name = serializers.CharField(required=True, max_length=50)
    account_created = serializers.CharField(required=False, default="", allow_null=True, allow_blank=True)
    aka_list = serializers.ListField(child=PeopleAKASerializer())

    fields_map = {
        'name': 'name',        
        'account_created': 'account_created'      
    }

    class Meta:
        model = People
        
    def validate_account_created(self, account):
        return self.context['request'].user

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_aka(validated_data, instance)

    def validate_aka_list(self, item_list):
        aka_list = filter(lambda x: x['name'], item_list)
        return list(aka_list)

    def create_aka(self,  validated_data, people):
        for aka in validated_data.get('aka_list', []):
            data = AKA.objects.filter(people=people, language=aka['language'])            
            if data.exists():
                data.update(name=aka['name'], language=aka['language'])
            else:
                AKA.objects.create(
                    people=people,
                    name=aka['name'],
                    language_id=aka['language'],
                    account_created=people.account_created
                )            
        return True