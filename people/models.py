from django.conf import settings
from django.db import models

from utils.fields import SaveMixin


class People(SaveMixin, models.Model):
    ACTIVE = 'Y'
    IN_ACTIVE = 'N'

    ACTIVE_CHOICES = (
        (ACTIVE, True),
        (IN_ACTIVE, False)
    )

    id = models.CharField(max_length=15, primary_key=True, db_column="PEOPLE_ID")
    name = models.CharField(max_length=50, null=True, db_column="PEOPLE_NAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    prefix = "PP"
    primary_key = "id"

    class Meta:
        ordering = ['name']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_PEOPLE')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)

    def __str__(self):
        return self.name

    @staticmethod
    def pull(pk):
        return People.objects.filter(pk=pk).first()

class AKA(models.Model):
    ACTIVE = 'Y'
    IN_ACTIVE = 'N'

    ACTIVE_CHOICES = (
        (ACTIVE, True),
        (IN_ACTIVE, False)
    )

    people = models.ForeignKey(
        People,
        primary_key=settings.IS_ORA,
        db_column="PEOPLE_ID",
        on_delete=models.CASCADE
    )
    language = models.ForeignKey(
        'language.Language',
        related_name="language",
        db_column="LANGUAGE_CODE",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=100, db_column="PEOPLE_NAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['language']
        managed = False
        db_table = settings.TABLE_NAME('STM_PEOPLE_AKA')
        default_permissions = ('view', 'add', 'change', 'delete',)
