from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import PeopleView

router = DefaultRouter()

router.register(r'', PeopleView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
