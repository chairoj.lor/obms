from language.models import Language
from .models import People, AKA


def init_people(id, name, language):
    People.objects.get_or_create(id=id)
    language = Language.objects.get(code=language)
    # print('init people', language)
    AKA.objects.get_or_create(people_id=id, name=name, language=language)
