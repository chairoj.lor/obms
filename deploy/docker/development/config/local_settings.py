DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'obms',
        'HOST': 'mysql',
        'PORT': 3306,
        'USER': 'stmcore',
        'PASSWORD': 'password'
    },
    'oracle': {
        'ENGINE': 'django.db.backends.oracle',
        'NAME': "(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = 10.18.14.234)(PORT = 1522)) (CONNECT_DATA = (SID = obmsdb) ) )",
        'USER': 'obmsapp',
        'PASSWORD': 'obms$user_01'

    }
}
