from rest_framework import serializers

from ..models import Channel


class ChannelManagementSerializer(serializers.ModelSerializer):
    account_created_id = serializers.IntegerField(write_only=True, required=False)
    account_updated_id = serializers.IntegerField(write_only=True, required=False)

    class Meta:
        model = Channel
        fields = (
            'code',
            'name',
            'bms_code',
            'definition_type',
            'source',
            'is_active',
            'is_catchup',
            'is_live_record',
            'is_blackout',
            'account_created_id',
            'account_updated_id',
        )

    def validate_account_created_id(self, account_created_id):
        view = self.context['view']
        request = self.context['request']
        if view.action == 'created':
            return request.user.id
        return None

    def validate_account_updated_id(self, account_updated_id):
        view = self.context['view']
        request = self.context['request']
        if view.action in ['update', 'partial_update']:
            return request.user.id
        return None

    def create(self, validated_data):
        request = self.context['request']
        validated_data.update({'account_created_id': request.user.id}, )
        return super().create(validated_data)

    def update(self, instance, validated_data):
        request = self.context['request']
        validated_data.update({'account_updated_id': request.user.id}, )
        return super().update(instance, validated_data)



