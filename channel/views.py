from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import mixins, viewsets

from .models import Channel
from .serializers import ChannelSerializer


class ChannelView(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = Channel.objects.filter(is_active='Y')
    serializer_class = ChannelSerializer
    pagination_class = None

    filter_backends = (
        DjangoFilterBackend,
    )

    filter_fields = (
        'is_catchup',
        'is_live_record',
        'is_blackout',
    )