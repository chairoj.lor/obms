from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter

from .views import DealView

router = DefaultRouter()

router.register(r'', DealView)

urlpatterns = [
    url(r'^', include(router.urls)),
]
