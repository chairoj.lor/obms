from rest_framework import exceptions, serializers

from config.models import System
from country.models import Country
from deal.models import Deal, Linear, Territory, Video
from episode.models import Episode
from title.models import Title
from utils.deal.queryset import is_period_conflict
from utils.rest_framework import serializer as utils_serializers
from vendor.models import Studio as VendorStudio, Vendor


class TitleOfFromSerializer(serializers.Serializer):
    title_id = serializers.CharField(max_length=15, required=True)
    title_name = serializers.CharField(max_length=50, required=True)
    title_type = serializers.CharField(max_length=15, required=True)
    total_episode = serializers.IntegerField(required=True)
    datetime_right_start = serializers.DateTimeField(required=True)
    datetime_right_end = serializers.DateTimeField(required=True)
    duration = serializers.CharField(max_length=6, required=True)

    def validate_title_id(self, title_id):
        print('title_id ', title_id)
        if Title.objects.filter(id=title_id).exists():
            return title_id
        raise exceptions.ValidationError('Title {} not found '.format(title_id))


class OtherItemSerializer(TitleOfFromSerializer):
    mg = serializers.IntegerField(required=False, default=0)
    deem_price = serializers.IntegerField(required=False, default=0)
    rev_share = serializers.IntegerField(required=False, default=0)

    def validate_title_id(self, title_id):
        return super().validate_title_id(title_id)


class LinerItemSerializer(TitleOfFromSerializer):
    max_run = serializers.IntegerField(default=0, allow_null=True)


class TerritoryFromSerializer(serializers.Serializer):
    country_code = serializers.CharField(max_length=4)
    country_name = serializers.CharField(max_length=15)

    def validate_country_code(self, code):
        if Country.objects.filter(code=code).exists():
            return code
        raise exceptions.ValidationError('Country {} is not found.'.format(code))


class OtherSerializer(serializers.Serializer):
    label = serializers.CharField(max_length=15)
    items = serializers.ListField(
        child=OtherItemSerializer(),
        allow_empty=True,
        allow_null=True,
    )

    def validate_label(self, label):
        if System.is_vod_type(label):
            return label
        raise exceptions.ValidationError('VOD type {} is not found'.format(label))


class DealFromSerializer(utils_serializers.ContentSerializer, serializers.Serializer):
    dea_no = serializers.CharField(max_length=15, required=False),
    ref_no = serializers.CharField(max_length=50, required=False, allow_null=True, allow_blank=True)
    status = serializers.CharField(max_length=20, required=True)
    remark = serializers.CharField(max_length=20, required=False, allow_null=True, allow_blank=True)
    attach = utils_serializers.PDFBase64File(allow_null=True, allow_empty_file=True,  default=None)
    studio_id = serializers.CharField(max_length=15, allow_blank=True, allow_null=True, default=None, required=False)
    vendor_id = serializers.CharField(max_length=15)
    bu_id = serializers.CharField(max_length=15)
    datetime_transaction = serializers.DateTimeField(allow_null=True, required=False)

    linear_list = serializers.ListField(
        child=LinerItemSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[],
    )
    other_list = serializers.ListField(
        child=OtherSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[],
    )
    territory_list = serializers.ListField(
        child=TerritoryFromSerializer(),
        required=False,
        allow_null=True,
        allow_empty=True,
        default=[]
    )

    fields_map = {
        'ref_no': 'ref',
        'bu_id': 'bu_id',
        'status': 'status',
        'remark': 'description',
        'studio_id': 'studio_id',
        'vendor_id': 'vendor_id',
        'attach': 'attach',
        'datetime_transaction': 'transaction',
        'account_created': 'account_created',
        'account_updated': 'account_updated',
    }

    class Meta:
        model = Deal


    def validate_vendor_id(self, vendor_id):
        if Vendor.objects.filter(id=vendor_id).exists():
            return vendor_id
        raise exceptions.ValidationError('vendor_id {} is not found.'.format(vendor_id))

    def create_relation(self, instance, validated_data, **kwargs):
        self.create_linear(validated_data.get('linear_list'), instance, **kwargs)
        self.create_video(validated_data.get('other_list'), instance, **kwargs)
        self.create_territory(validated_data, instance, **kwargs)

    def create_territory(self, validate_date, instance, **kwargs):
        if Territory.objects.filter(deal=instance):
            Territory.objects.filter(deal=instance).delete()
        for territory_from in validate_date.get('territory_list', []):
            Territory.objects.create(
                deal=instance,
                country_id=territory_from['country_code'],
                account_created=kwargs['account'],
            )

    def create_linear(self, linear_list, deal, **kwargs):
        view = self.context['view']
        if view.action in ['update', 'partial_update']:
            Linear.objects.filter(deal=deal).delete()
        if len(linear_list) > 0:
            for index, item in enumerate(linear_list):
                Linear.objects.create(
                    deal=deal,
                    title_id=item['title_id'],
                    datetime_right_start=item['datetime_right_start'],
                    datetime_right_end=item['datetime_right_end'],
                    max_run=item['max_run'],
                    no=index + 1,
                    account_created=kwargs['account']
                )

    def create_video(self, other_list, deal, **kwargs):
        view = self.context['view']
        if view.action in ['update', 'partial_update']:
            Video.objects.filter(deal=deal).delete()

        if len(other_list) > 0:
            for index, item in enumerate(other_list):
                Video.objects.create(
                    deal=deal,
                    title_id=item['title_id'],
                    datetime_right_start=item['datetime_right_start'],
                    datetime_right_end=item['datetime_right_end'],
                    type=item['type'],
                    mg=item['mg'],
                    deem_price=item['deem_price'],
                    rev_share=item['rev_share'],
                    no=index + 1,
                    account_created=kwargs['account']
                )

    def validate_period_list(self, item_list, queryset):
        for index_of_title, title in enumerate(item_list):
            if title['datetime_right_start'] > title['datetime_right_end']:
                raise exceptions.ValidationError(
                    'Title `{}` datetime_start getter datetime_right_end'.format(title['title_name']))
            if is_period_conflict(
                    title['datetime_right_start'],
                    title['datetime_right_end'],
                    queryset.filter(title_id=title['title_id'])
            ):
                raise exceptions.ValidationError(
                    'Title `{}` datetime_right_start and datetime_right_end to conflict (current)'.format(title['title_name'])
                )
            if not Episode.is_period_use(title['title_id'], title['datetime_right_start'], title['datetime_right_end']):
                raise exceptions.ValidationError(
                    'Episode of title `{}` datetime_right_start and datetime_right_end to usege '.format(
                        title['title_name'])
                )

            next_index = index_of_title + 1
            if next_index >= len(item_list):
                break
            title_next = item_list[next_index]
            if title_next['datetime_right_start'] < title['datetime_right_end']:
                raise exceptions.ValidationError(
                    'Title `{}` datetime_right_start and datetime_right_end to conflict (next)'.format(title_next['title_name'])
                )
            if not Episode.is_period_use(title_next['title_id'], title_next['datetime_right_start'], title_next['datetime_right_end']):
                raise exceptions.ValidationError(
                    'Episode of title `{}` datetime_right_start and datetime_right_end to usege '.format(
                        title['title_name'])
                )
            if is_period_conflict(
                    title_next['datetime_right_start'],
                    title_next['datetime_right_end'],
                    queryset.filter(title_id=title_next['title_id'])
            ):
                raise exceptions.ValidationError(
                    'Title `{}` datetime_right_start and datetime_right_end to conflict'.format(title_next['title_name'])
                )

    def validate_status(self, status):
        if System.is_exist(status, 'DEAL_STATUS'):
            return status
        raise exceptions.ValidationError('Status `{}` is not found.'.format(status))

    def validate_and_convert(self, title_list, queryset):
        items = []
        for item_list in title_list.values():
            item_list.sort(key=lambda title: title['datetime_right_start'])
            item_list = list(item_list)
            self.validate_period_list(item_list, queryset)
            items = items + item_list
        if None in items:
            print('Title is empty ', items)
        return items

    def validate_other_list(self, other_list):
        title_list = {}
        for other in other_list:
            label = other.get('label', None)
            for item in other.get('items', []):
                key = item.get('title_id')
                if key and label:
                    item.update({'type': label})
                    if key in title_list:
                        item_list = title_list[key]
                        item_list.append(item)
                    else:
                        title_list.update({key: [item]})

        view = self.context['view']
        deal = None
        if view.action in ['update', 'partial_update']:
            deal = view.get_object()
        queryset = Video.objects.filter(title_id__in=list(title_list.keys())).exclude(deal=deal)
        return self.validate_and_convert(title_list, queryset)

    def validate_linear_list(self, linear_list):
        title_list = {}
        for linear in linear_list:
            key = linear.get('title_id')
            if key:
                if key in title_list:
                    item_list = title_list[key]
                    item_list.append(linear)
                else:
                    title_list.update({key: [linear]})

        view = self.context['view']
        deal = None
        if view.action in ['update', 'partial_update']:
            deal = view.get_object()
        queryset = Linear.objects.filter(title_id__in=list(title_list.keys())).exclude(deal=deal)
        return self.validate_and_convert(title_list, queryset)

    def validate(self, attrs):
        if 'vendor_id' in attrs and 'studio_id' in attrs:
            if not VendorStudio.objects.filter(vendor_id=attrs['vendor_id'], studio_id=attrs['studio_id']).exists():
                raise exceptions.ValidationError(detail={'studio_id': 'Studio id is not found on vendor'})
        return super().validate(attrs)

