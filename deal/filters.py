from django.db.models import Q
from django_filters import filters
from django_filters.rest_framework import FilterSet

from .models import Deal


class DealFilter(FilterSet):
    datetime_right_start = filters.DateTimeFilter(
        method="get_datetime_right_start",
        field_name="datetime_right_start"
    )
    datetime_right_end = filters.DateTimeFilter(
        method="get_datetime_end",
        field_name="datetime_right_end"
    )
    vendor_name = filters.CharFilter(
        method='get_vendor_name',
        field_name='vendor_name'
    )
    studio_name = filters.CharFilter(
        method='get_studio_name',
        field_name='studio_name',
    )

    class Meta:
        model = Deal
        fields = (
            'studio_id',
            'studio_name',
            'vendor_id',
            'vendor_name',
            'status',
            'datetime_right_start',
            'datetime_right_end'
        )

    def get_datetime_right_start(self, queryset, name, value):
        return queryset.filter(
            Q(linear__datetime_right_start__gte=value) | Q(video__datetime_right_start__gte=value)
        )

    def get_datetime_end(self, queryset, name, value):
        return queryset.filter(Q(linear__datetime_right_end__lte=value) | Q(linear__datetime_right_end__lte=value))

    def get_vendor_name(self, queryset, name, value):
        return queryset.filter(
            vendor__name=value
        )

    def get_studio_name(self, queryset, name, value):
        return queryset.filter(
            studio__name=value
        )
