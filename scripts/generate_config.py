import os
import sys

import django

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "bms.settings")
django.setup()

from config.models import System

if __name__ == '__main__':
    pass
    # System.gen_vod_type()
