from django.urls import include, path
from rest_framework import routers

from .views import RatingView

router = routers.DefaultRouter()
router.register(r'', RatingView)

app_name = 'rating'
urlpatterns = [    
    path('', include(router.urls)),
]
