from rest_framework import exceptions, status, viewsets, filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .serializers import OwnerSerializer, OwnerListSerializer
from ..models import BusinessOwner
from .filters import OwnerFilter

class OwnerView(viewsets.ModelViewSet):
    queryset = BusinessOwner.objects.all()
    serializer_class = OwnerListSerializer

    app = 'owner'
    model = 'businessowner'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': OwnerListSerializer,
        'create': OwnerSerializer,
        'update': OwnerSerializer,
        'partial_update': OwnerSerializer,
        'destroy': OwnerListSerializer,
    }

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = OwnerFilter

    search_fields = (
        'id',
        'name'        
    )

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        vendor = self.perform_create(serializer)
        serializer_response = OwnerSerializer(vendor)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(err)
            raise exceptions.ValidationError()
