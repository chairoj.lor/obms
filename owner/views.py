from rest_framework import viewsets

from .models import BusinessOwner
from .serializers import BusinessOwnerSerializer


class BusinessOwnerView(viewsets.ReadOnlyModelViewSet):
    queryset = BusinessOwner.objects.all()
    serializer_class = BusinessOwnerSerializer
    pagination_class = None
