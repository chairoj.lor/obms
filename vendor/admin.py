from django.contrib import admin

from .models import Studio, Vendor


@admin.register(Vendor)
class VendorModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name',)
    search_fields = ['id', 'name']

@admin.register(Studio)
class StudioModelAdmin(admin.ModelAdmin):
    list_display = ('vendor_id', 'studio_id',)
    search_fields = ['vendor_id', 'studio_id']
