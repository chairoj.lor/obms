# Generated by Django 3.0 on 2020-06-25 06:47

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vendor', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Studio',
            fields=[
                ('vendor', models.ForeignKey(db_column='VENDOR_ID', on_delete=django.db.models.deletion.CASCADE, primary_key=settings.IS_ORA, related_name='vendor_studio_set', serialize=False, to='vendor.Vendor')),
                ('studio',
                 models.ForeignKey(db_column='STUDIO_ID', on_delete=django.db.models.deletion.CASCADE, related_name='studio_vendor_set', serialize=False, to='studio.Studio')),
            ],
            options={
                'db_table': 'STM_VENDOR_STUDIO',
                'managed': settings.IS_MIGRATE,
                'default_permissions': ('view', 'view_detail', 'add', 'change', 'delete'),
            },
        ),
    ]
