from rest_framework import serializers

from language.serializers import LanguageSerializer
from .models import AKA, Studio


class StudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Studio
        fields = (
            'id',
            'name',
        )


class StudioListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Studio
        fields = (
            'id',
            'name',
        )


class AKASerializer(serializers.ModelSerializer):

    class Meta:
        model = AKA
        fields = (
            'studio',
            'name',
            'language',
            # 'account_created',
            # 'datetime_created'
        )


class AKAListSerializer(serializers.ModelSerializer):
    # create_by = AccountSerializer(source='account_created', many=False, read_only=True)
    studio_name = StudioSerializer(source='studio.name', many=False, read_only=True)
    language = serializers.SerializerMethodField()

    class Meta:
        model = AKA
        fields = (
            # 'id',
            'studio',
            'studio_name',
            'name',
            'language',
            # 'account_created',
            # 'datetime_created',
            # 'create_by'
        )

    def get_language(self, aka):
        return LanguageSerializer(aka.language).data

