from django.conf import settings
from django.db import models

from utils.fields import ObmModel, SaveMixin


class Studio(SaveMixin, models.Model):
    id = models.CharField(max_length=15, primary_key=True, db_column="STUDIO_ID")
    name = models.CharField(max_length=50, db_column="STUDIO_NAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="+",
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    prefix = "SJ"
    primary_key = "id"

    class Meta:
        ordering = ('name', )
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_STUDIO')
        default_permissions = ('view', 'add', 'change', 'delete',)

    def __str__(self):
        return '%s (%s)' % (self.id, self.name)

    @staticmethod
    def pull(pk):
        return Studio.objects.filter(pk=pk).first()


class AKA(models.Model):

    studio = models.ForeignKey(
        Studio,
        primary_key=settings.IS_ORA,
        db_column="STUDIO_ID",
        on_delete=models.CASCADE
    )

    language = models.ForeignKey(
        'language.Language',
        related_name='+',
        db_column="LANGUAGE_CODE",
        on_delete=models.CASCADE,
    )

    name = models.CharField(max_length=100, db_column="STUDIO_NAME")
    datetime_created = models.DateTimeField(auto_now_add=True, db_column="CREATED_DATE")
    account_created = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        db_column="CREATED_BY",
        on_delete=models.CASCADE
    )

    class Meta:
        ordering = ['-datetime_created']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_STUDIO_AKA')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)
