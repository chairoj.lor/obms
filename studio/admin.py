from django.contrib import admin

from .models import Studio, AKA


@admin.register(Studio)
class StudioModelAdmin(admin.ModelAdmin):
    list_display = (
        'id',
    )
    search_fields = ['id']


@admin.register(AKA)
class AKAModelAdmin(admin.ModelAdmin):
    list_display = ('name', 'studio', 'language')
    # raw_id_fields = ('studio', 'language')
