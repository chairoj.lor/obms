from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import exceptions, filters, status, viewsets
from rest_framework.response import Response

from utils.rest_framework.permission import AddPermission, ChangePermission, DeletePermission, ViewPermission
from .filters import StudioFilter
from .serializers import StudioCreateSerializer, StudioSerializer
from ..models import Studio
from .filters import StudioFilter


class StudioView(viewsets.ModelViewSet):
    queryset = Studio.objects.all()
    serializer_class = StudioSerializer
    app = 'studio'
    model = 'studio'

    permission_classes_action = {
        'list': [ViewPermission],
        'create': [AddPermission],
        'retrieve': [ViewPermission],
        'update': [ChangePermission],
        'partial_update': [ChangePermission],
        'destroy': [DeletePermission],
    }

    action_serializers = {
        'list': StudioSerializer,
        'create': StudioCreateSerializer,
        'update': StudioCreateSerializer,
        'partial_update': StudioCreateSerializer,
        'destroy': StudioSerializer,
    }

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        DjangoFilterBackend,
    )

    filter_class = StudioFilter

    search_fields = (
        'id',
        'name'        
    )

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
        return super().get_serializer_class()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        studio = self.perform_create(serializer)
        serializer_response = StudioSerializer(studio)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):        
        partial = kwargs.pop('partial', False)
        instance = self.get_object()        
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        serializer_response = StudioSerializer(instance)
        return Response(serializer_response.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        return serializer.save()

    def perform_destroy(self, instance):
        try:
            super().perform_destroy(instance)
        except Exception as err:
            print(err)
            raise exceptions.ValidationError()
