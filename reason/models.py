from django.conf import settings
from django.db import models

from utils.fields import ObmModel


class Censor(ObmModel):
    id = models.CharField(max_length=15, primary_key=True, db_column="REASON_ID")
    description = models.CharField(max_length=50, null=True, blank=True, db_column="DESCRIPTION")

    primary_key = "id"

    prefix = "RES"

    class Meta:
        ordering = ['-datetime_created']
        managed = settings.IS_MIGRATE
        db_table = settings.TABLE_NAME('STM_REASON_CENSOR')
        default_permissions = ('view', 'view_detail', 'add', 'change', 'delete',)
