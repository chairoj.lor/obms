# Generated by Django 3.0 on 2020-06-25 09:25

import django.db.models.deletion
from django.conf import settings
from django.db import migrations, models

import utils.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('title','0001_initial'),
        ('language', '0001_initial'),
        ('publish', '0001_initial'),

    ]

    operations = [
        migrations.CreateModel(
            name='Episode',
            fields=[
                ('datetime_updated', models.DateTimeField(auto_now=True, db_column='MODIFIED_DATE', null=True)),
                ('datetime_created', models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')),

                ('datetime_expect_receive', models.DateTimeField(default=None, db_column='EXPECT_RECEIVE_DATE', null=True)),
                ('datetime_receive', models.DateTimeField(default=None, db_column='RECEIVE_DATE', null=True)),

                ('id', models.CharField(db_column='EPISODE_ID', max_length=15, primary_key=True, serialize=False)),
                ('title', models.ForeignKey(db_column='TITLE_ID', on_delete=django.db.models.deletion.CASCADE, serialize=False, to='title.Title')),
                ('name', models.CharField(db_column='EPISODE_NAME', max_length=50)),
                ('duration', models.CharField(db_column='DURATION', max_length=6)),
                ('asset_id', models.CharField(db_column='MAIN_ASSET_ID', max_length=15, null=True, blank=True, default=None)),

                ('pac_no', models.IntegerField(db_column='PAC_NO', default=None, null=True)),
                ('use_run', models.IntegerField(db_column='USE_RUN', default=0, null=True)),
                ('account_created',
                 models.ForeignKey(db_column='CREATED_BY', on_delete=django.db.models.deletion.CASCADE,
                                   related_name='+', to=settings.AUTH_USER_MODEL)),
                ('account_updated',
                 models.ForeignKey(db_column='MODIFIED_BY', on_delete=django.db.models.deletion.SET_NULL, null=True,
                                   default=None,
                                   related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'STM_EPISODE',
                'ordering': ['-id'],
                'managed': settings.IS_MIGRATE,
                'default_permissions': ('view', 'view_detail', 'add', 'change', 'delete'),
            },
            bases=(utils.fields.SaveMixin, models.Model),
        ),
        migrations.CreateModel(
            name='AKAEpisode',
            fields=[
                ('episode', models.ForeignKey(db_column='EPISODE_ID', on_delete=django.db.models.deletion.CASCADE, primary_key=settings.IS_ORA, serialize=False, to='episode.Episode')),
                ('name', models.CharField(db_column='EPISODE_NAME', default=None, max_length=200, null=True)),
                ('synopsis', models.CharField(db_column='SYNOPSIS', default=None, max_length=200, null=True)),
                ('language', models.ForeignKey(db_column='LANGUAGE_CODE', on_delete=django.db.models.deletion.CASCADE, related_name="aka_episode_language_set", serialize=False, to='language.Language')),
                ('datetime_created', models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')),
                ('account_created',
                 models.ForeignKey(db_column='CREATED_BY', on_delete=django.db.models.deletion.CASCADE,
                                   related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'STM_EPISODE_AKA',
                'managed': settings.IS_MIGRATE,
                'default_permissions': ('view', 'view_detail', 'add', 'change', 'delete'),
            },
        ),
        migrations.CreateModel(
            name='PublishEpisode',
            fields=[
                ('episode', models.ForeignKey(db_column='EPISODE_ID', on_delete=django.db.models.deletion.CASCADE, primary_key=settings.IS_ORA, serialize=False, to='episode.Episode')),
                ('sequence_no', models.IntegerField(db_column='SEQ_NO')),
                ('datetime_start', models.DateTimeField(db_column='PUBLISH_START', default=None, null=True)),
                ('datetime_end', models.DateTimeField(db_column='PUBLISH_END', default=None, null=True)),
                ('datetime_created', models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')),
                ('publish', models.ForeignKey(db_column='PUBLISH_ZONE_ID', on_delete=django.db.models.deletion.CASCADE,
                                               related_name="publish_set", serialize=False,
                                               to='publish.Publish')),

                ('account_created',
                 models.ForeignKey(db_column='CREATED_BY', on_delete=django.db.models.deletion.CASCADE,
                                   related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'STM_EPISODE_PUBLISH',
                'ordering': ('-episode_id',),
                'managed': settings.IS_MIGRATE,
                'default_permissions': ('view', 'add', 'change', 'delete'),
            },
        ),
        migrations.CreateModel(
            name='Segment',
            fields=[
                ('episode', models.ForeignKey(db_column='EPISODE_ID', on_delete=django.db.models.deletion.CASCADE, primary_key=settings.IS_ORA, serialize=False, to='episode.Episode')),
                ('segment_no', models.IntegerField(db_column='SEGMENT_NO')),
                ('duration', models.CharField(db_column='DURATION', max_length=6)),
                ('datetime_created', models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')),
                ('account_created',
                 models.ForeignKey(db_column='CREATED_BY', on_delete=django.db.models.deletion.CASCADE,
                                   related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'STM_EPISODE_SEGMENT',
                'managed': settings.IS_MIGRATE,
                'default_permissions': ('view', 'add', 'change', 'delete'),
            },
        ),
        migrations.CreateModel(
            name='ThumbnailEpisode',
            fields=[
                ('episode', models.ForeignKey(db_column='EPISODE_ID', on_delete=django.db.models.deletion.CASCADE, primary_key=settings.IS_ORA, serialize=False, to='episode.Episode')),
                ('tag', models.CharField(db_column='TAG', max_length=200)),
                ('file', models.FileField(db_column='FILENAME', upload_to='thumbnail/')),
                ('datetime_created', models.DateTimeField(auto_now_add=True, db_column='CREATED_DATE')),
                ('account_created',
                 models.ForeignKey(db_column='CREATED_BY', on_delete=django.db.models.deletion.CASCADE,
                                   related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'STM_EPISODE_THUMNAILS',
                'managed': settings.IS_MIGRATE,
                'default_permissions': ('view', 'add', 'change', 'delete'),
            },
        ),
    ]
