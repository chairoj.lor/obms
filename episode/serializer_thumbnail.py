from rest_framework import serializers

from utils.rest_framework.serializer import Base64ImageField


class EpisodeThumbFromSerializer(serializers.Serializer):
    tag = serializers.CharField(
        required=True,
        max_length=15
    )
    text_source = Base64ImageField(
        required=True
    )

