from django.contrib import admin

from .models import AKAEpisode, Episode, PublishEpisode, Segment, ThumbnailEpisode


@admin.register(Episode)
class EpisodeModelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'title_id')
    actions = ['active']

    def active(self, request, queryset):
        queryset.all()


@admin.register(AKAEpisode)
class AKAEpisodeModelAdmin(admin.ModelAdmin):
    list_display = ('episode_id', 'name', 'language_id',)


@admin.register(PublishEpisode)
class PublishEpisodeModelAdmin(admin.ModelAdmin):
    list_display = ('episode_id', 'sequence_no', 'publish_id', 'datetime_start', 'datetime_end')


@admin.register(ThumbnailEpisode)
class ThumbnailEpisodeModelAdmin(admin.ModelAdmin):
    list_display = ('episode_id', 'tag', 'file')


@admin.register(Segment)
class SegmentEpisodeModelAdmin(admin.ModelAdmin):
    list_display = ('segment_no', 'duration', 'episode_id')
