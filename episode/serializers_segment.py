from rest_framework import serializers


class EpisodeSegmentFromSerializer(serializers.Serializer):
    segment_no = serializers.IntegerField(required=True)
    duration = serializers.CharField(required=True, max_length=6)
